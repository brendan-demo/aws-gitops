# aws-gitops

## Getting Started

### Prerequisites
* AWS, Terraform Cloud accounts
* AWS IAM Access Key & Secert Access Key
* 
* An AWS Key Pair for your EC2 instances

### Setup
* Connect [Terraform cloud to your GitLab.com account]()
* Add the `AWS_ACCESS_KEY_ID` and `AWS_SECRET_ACCESS_KEY` to the Variables section of your Terraform Cloud workspace
* Add the key pair name to `key_name` in the Terraform Variables section of your Terraform Cloud workspace
* Add your public IP address to `admin_cidr_ingress` in the Terraform Variables section of your Terraform Cloud workspace


### Plan (local)
To plan the deploy locally, you can run

```
terraform plan \
	-var admin_cidr_ingress='"{your_ip_address}/32"' \
	-var key_name={your_key_name}

```

> Inspired by https://github.com/terraform-providers/terraform-provider-aws/tree/master/examples/ecs-alb.